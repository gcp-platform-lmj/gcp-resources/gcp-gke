# this file contains provider details
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      #version = "4.51.0"
    }
  }
}

# we can add provider specific configuration here
provider "google" {
  #credentials = "${file("./cred/project-gke-2023-d3891a97d5d7.json")}"
  #credentials = ${SERVICE_ACCOUNT_KEY}
  project = "project-gke-2023"
  region  = "asia-east1"
  zone    = "asia-east1-a"
}


data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}