## This is main.tf file contains cluster def and node pool def - this is a comment
locals {
  project                        = "project-gke-2023"
  network_name                   = "vpc-demo"
  k8s_subnet                     = "subnet-k8s"
  location                       = "asia-east1-a"
  k8s_node_service_account       = "gke-node-sa@project-gke-2023.iam.gserviceaccount.com"
  k8s_name                       = "my-first-k8s"
  k8s_master_ipv4_cidr           = "172.16.0.96/28"
  k8s_pod_range                  = "10.0.1.0/24"
  k8s_service_range              = "10.0.2.0/26"
  k8s_disk_size                  = 30
  k8s_master_authorized_networks = "10.0.10.20/19"

}

module "demo-k8s" {
  source     = "git::https://gitlab.com/gcp-platform-lmj/tf-modules/learngcp.git//modules/gke-cluster-standard"
  project_id = local.project
  name       = local.k8s_name
  location   = local.location
  vpc_config = {
    network    = local.network_name
    subnetwork = local.k8s_subnet
    secondary_range_names = {
      pods     = "pod-range"
      services = "service-range"
    }
    k8s_master_authorized_ranges = {
      internal-vms = local.k8s_master_authorized_networks

    }
    master_ipv4_cidr_block = local.k8s_master_ipv4_cidr
  }
}

module "cluster-node-pool-1" {
  source       = "git::https://gitlab.com/gcp-platform-lmj/tf-modules/learngcp.git//modules/gke-nodepool"
  depends_on   = [module.demo-k8s]
  name         = "k8s-node-pool-01"
  project_id   = local.project
  cluster_name = local.k8s_name
  location     = local.location
  labels       = { environment = "dev" }
  service_account = {
    name         = local.k8s_node_service_account
    oauth_scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }
  node_config = {
    machine_type    = "e2-medium"
    spot            = false
    disk_size_gb    = local.k8s_disk_size
    disk_type       = "pd-standard"
    image_type      = "COS_CONTAINERD"
    gvnic           = false
    preemptible     = true
    auto_repair     = true
    auto_upgrade    = true
    nodepool_config = {
      autoscaling = {
        max_node_count = 2
        min_node_count = 1
      }
      management = {
        auto_repair  = true
        auto_upgrade = false
      }
    }
  }
}

# node_pools_oauth_scopes = {
#   all = [
#     "https://www.googleapis.com/auth/logging.write",
#     "https://www.googleapis.com/auth/monitoring",
#   ]
#}
# Added below content if required. 
#node_pools_labels = {
#  all = {}
#
#  default-node-pool = {
#      default-node-pool = true
#}
#}
#
#node_pools_metadata = {
#   all = {}
#
#   default-node-pool = {
#      node-pool-metadata-custom-value = "my-node-pool"
#}
#}
#
#node_pools_taints = {
#   all = []
#
#   default-node-pool = [
#      {
#        key    = "default-node-pool"
#        value  = true
#        effect = "PREFER_NO_SCHEDULE"
#  },
#]
#}

#node_pools_tags = {
#   all = []
#
#   default-node-pool = [
#      "default-node-pool",
#]
#}
#}